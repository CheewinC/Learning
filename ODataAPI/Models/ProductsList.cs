﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ODataAPI.Models
{
    public class ProductsList
    {
        public List<Products> Products { get; set; }
    }
}
