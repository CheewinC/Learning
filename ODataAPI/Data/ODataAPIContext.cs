﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ODataAPI.Models;

namespace ODataAPI.Data
{
    public class ODataAPIContext : DbContext
    {
        public ODataAPIContext (DbContextOptions<ODataAPIContext> options)
            : base(options)
        {
        }

        public DbSet<ODataAPI.Models.Products> Products { get; set; }
    }
}
