﻿using Microsoft.AspNet.OData.Builder;
using Microsoft.OData.Edm;
using ODataAPI.Models;
using System;
namespace ODataAPI.ModelsBuilder
{
    public class ProductsModelBuilder
    {
        public IEdmModel GetEdmModel(IServiceProvider serviceProvider)
        {
            var builder = new ODataConventionModelBuilder(serviceProvider);
            builder.EntitySet<Products>("Products")
                .EntityType
                .Filter()
                .Count()
                .Expand()
                .OrderBy()
                .Page()
                .Select();
            return builder.GetEdmModel();
        }
    }
}
