using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using ODataAPI.Data;
using Microsoft.AspNet.OData.Extensions;
using ODataAPI.ModelsBuilder;
using Microsoft.AspNet.OData.Builder;
using ODataAPI.Models;

namespace ODataAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(mvcOptions =>
                mvcOptions.EnableEndpointRouting = false).AddNewtonsoftJson() ;
            
            services.AddDbContext<ODataAPIContext>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("ODataAPIContext")));

            services.AddOData();
            //services.AddTransient<ProductsModelBuilder>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env/*, ProductsModelBuilder productsModelBuilder*/)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            var builder = new ODataConventionModelBuilder(app.ApplicationServices);

            //builder.EntitySet<ProductsList>("products");
            //var movieStar = builder.EntitySet<Products>("Products").EntityType;


            app.UseMvc(routeBuilder =>
            {
                routeBuilder.EnableDependencyInjection();
                routeBuilder.Expand().Select().Count().OrderBy().Filter().MaxTop(100);

                routeBuilder.MapODataServiceRoute("ODataRoute", "odata", builder.GetEdmModel());
                //routeBuilder.MapODataServiceRoute("api", "api", productsModelBuilder.GetEdmModel(app.ApplicationServices));
            });

            app.UseAuthorization();

            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapControllers();
            //});
        }
    }
}
