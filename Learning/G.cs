﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;

namespace Learning
{
    public class G
    {
        public static void Alert(Page p, string txt)
        {
            txt = txt.Replace("'", "\"");
            txt = txt.Replace("\r", "");
            txt = txt.Replace("\n", "");
            ScriptManager.RegisterStartupScript(p, p.GetType(), "DotNetAlert", "alert('" + txt + "');", true);
        }

        public static void AlertAndRedirect(Page p, string txt, string url)
        {
            txt = txt.Replace("'", "\"");
            txt = txt.Replace("\r", "");
            txt = txt.Replace("\n", "");
            ScriptManager.RegisterStartupScript(p, p.GetType(), "DotNetAlert", "alert('" + txt + "'); window.location.href = '" + url + "';", true);
        }

        public static bool IsValidEmail(string email)
        {
            try
            {
                MailAddress addr = new MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        public static void SendEmail(string receiverEmail, string subject, string body, string attachment = null)
        {
            if (string.IsNullOrWhiteSpace(attachment))
            {
                SendEmail(receiverEmail, subject, body, new List<string>());
            }
            else
            {
                SendEmail(receiverEmail, subject, body, new List<string> { attachment });
            }
        }
        public static void SendEmail(string receiverEmail, string subject, string body, List<string> attachments, string bccEmail = null)
        {
            if (IsValidEmail(receiverEmail))
            {
                #region Settings sender mail.

                string host;
                int port;
                bool enableSsl;
                string senderEmail, senderDisplayName, senderPassword;
                int emailTimeout;

                host = "smtp.office365.com";
                port = 587;
                enableSsl = true;
                senderEmail = "cheewin.c@outlook.com";
                senderPassword = "ecc2538";
                senderDisplayName = "Demo Email";
                emailTimeout = 20000;

                MailMessage mail = new MailMessage
                {
                    From = new MailAddress(senderEmail, senderDisplayName),
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                };

                SmtpClient smtp = new SmtpClient
                {
                    Host = host,
                    Port = port,
                    EnableSsl = enableSsl,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(senderEmail, senderPassword),
                    Timeout = emailTimeout
                };
                #endregion
                mail.To.Add(receiverEmail);

                if (bccEmail != null && IsValidEmail(bccEmail))
                    mail.Bcc.Add(bccEmail);

                foreach (string attachment in attachments)
                    mail.Attachments.Add(new Attachment(attachment));

                smtp.Timeout = 60000;
                smtp.Send(mail);
            }
            else
            {
                throw new Exception("Invalid email.");
            }
        }
    }
}