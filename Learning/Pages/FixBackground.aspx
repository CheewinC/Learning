﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FixBackground.aspx.cs" Inherits="Learning.Pages.FixBackground" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />

    <title></title>
    <style>
        html,
        body {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        html {
            scroll-behavior: smooth;
        }

        .bgPage {
            position: fixed;
            top: -50%;
            left: -50%;
            width: 200%;
            height: 200%;
            z-index: -999;
            transform: matrix(1, 0, 0, 1, 0, 0);
        }

            .bgPage img {
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                margin: auto;
                min-width: 50%;
                min-height: 50%
            }

            /*.bgPage img {
                position: absolute;
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover;
                width: 100%;
                height: 100%;
                margin: auto
            }*/

        .navbar {
            font-family: "Mitr", sans-serif !important;
            font-size: 17.5px;
            transition: all 0.4s;
        }

            .navbar .logo {
                width: 3rem;
                transition: all 0.4s;
            }

            .navbar.compressed .logo {
                width: 3.3rem;
            }

            .navbar.compressed {
                font-size: 19.5px;
                border-bottom: 0px;
                box-shadow: 0 0 50px 5px rgba(0, 0, 0, 0.3);
                background-color: #fff;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/jquery-3.4.1.slim.min.js" />
                <asp:ScriptReference Path="~/Scripts/popper.min.js" />
                <asp:ScriptReference Path="~/Scripts/bootstrap.min.js" />
            </Scripts>
        </asp:ScriptManager>
        <div class="bgPage">
            <img src="../images/bg03.jpg" />
        </div>
        <nav class="navbar fixed-top navbar-expand-md navbar-light bg-white border-0">
            <div class="container">
                <a class="navbar-brand" href="#">
                    <img src="../images/AgentDefault.jpg" alt="logo" class="logo" />
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                    <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Link</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Special title treatment</h5>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Special title treatment</h5>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
    </form>
    <script>
        $(document).ready(function () {
            $(window).on("scroll", function () {
                if ($(window).scrollTop() >= 20) {
                    $(".navbar").addClass("compressed");
                } else {
                    $(".navbar").removeClass("compressed");
                }
            });
        });
        //window.onscroll = function () {
        //    scrollFunction()
        //};

        //function scrollFunction() {
        //    if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 1500) {
        //        mybutton.style.display = "block";
        //    } else {
        //        mybutton.style.display = "none";
        //    }
        //};
    </script>
</body>
</html>
