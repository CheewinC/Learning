﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Learning.Pages
{
    public partial class DatePicker : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Label1.Text = TextBox1.Text;
            Label2.Text = txtDatePicker.Text;
            Label3.Text = uxDateTimeLocalTextbox.Value;
            ConvertStringToDate(TextBox1.Text, out DateTime dt1);
            ConvertStringToDate(txtDatePicker.Text, out DateTime dt);
        }

        public static bool ConvertStringToDate(string dateText, out DateTime date)
        {
            return DateTime.TryParseExact(dateText, "yyyy-MM-dd", new CultureInfo("en-GB"), DateTimeStyles.None, out date);
        }
    }
}