﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="materialize.aspx.cs" Inherits="Learning.Pages.materialize" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/jquery-3.4.1.min.js" />
                <asp:ScriptReference Path="~/Scripts/popper.min.js" />
                <asp:ScriptReference Path="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js" />
            </Scripts>
        </asp:ScriptManager>
        <asp:UpdatePanel runat="server" ID="udp1">
            <ContentTemplate>
                <nav>
                    <div class="nav-wrapper pink darken-1">
                        <a href="#!" class="brand-logo"><i class="material-icons">cloud</i>Logo</a>
                        <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                        <ul class="right hide-on-med-and-down">
                            <li><a href="sass.html">Sass</a></li>
                            <li><a href="badges.html">Components</a></li>
                            <li><a href="collapsible.html">Javascript</a></li>
                            <li><a href="mobile.html">Mobile</a></li>
                            <!-- Dropdown Trigger -->
                            <li><a class="dropdown-trigger" href="#" data-target="dropdown1">Dropdown<i class="material-icons right">arrow_drop_down</i></a></li>
                        </ul>
                    </div>
                </nav>

                <ul class="sidenav" id="mobile-demo">
                    <li><a href="sass.html">Sass</a></li>
                    <li><a href="badges.html">Components</a></li>
                    <li><a href="collapsible.html">Javascript</a></li>
                    <li><a href="mobile.html">Mobile</a></li>
                    <li><a class="dropdown-trigger" href="#" data-target="dropdown2">Dropdown<i class="material-icons right">arrow_drop_down</i></a></li>
                </ul>

                <ul id="dropdown1" class="dropdown-content">
                    <li><a href="#!">one</a></li>
                    <li><a href="#!">two</a></li>
                    <li class="divider"></li>
                    <li><a href="#!">three</a></li>
                </ul>
                <ul id="dropdown2" class="dropdown-content">
                    <li><a href="#!">one</a></li>
                    <li><a href="#!">two</a></li>
                    <li class="divider"></li>
                    <li><a href="#!">three</a></li>
                </ul>

                <br />
                <br />
                <br />
                <ul class="collapsible">
                    <li>
                        <div class="collapsible-header"><i class="material-icons">filter_drama</i>First</div>
                        <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
                    </li>
                    <li>
                        <div class="collapsible-header"><i class="material-icons">place</i>Second</div>
                        <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
                    </li>
                    <li>
                        <div class="collapsible-header"><i class="material-icons">whatshot</i>Third</div>
                        <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
                    </li>
                </ul>
                <br />
                <div class="container">
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s6">
                                <asp:TextBox ID="TextBox1" runat="server" CssClass="validate" placeholder="Placeholder"></asp:TextBox>
                                <%--<input placeholder="Placeholder" id="first_name" type="text" class="validate">--%>
                                <label for="first_name">First Name</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="last_name" type="text" class="validate">
                                <label for="last_name">Last Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input disabled value="I am not editable" id="disabled" type="text" class="validate">
                                <label for="disabled">Disabled</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="password" type="password" class="validate">
                                <label for="password">Password</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <asp:TextBox ID="TextBox2" runat="server" CssClass="validate" TextMode="Email"></asp:TextBox>
                                <%--<input id="email" type="email" class="validate">--%>
                                <label for='<%=TextBox2.ClientID %>'>Email</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                This is an inline input field:
                                  <div class="input-field inline">
                                      <%--<input id="email_inline" type="email" class="validate">--%>
                                      <asp:TextBox ID="TextBox3" runat="server" CssClass="validate" TextMode="Email"></asp:TextBox>
                                      <label for='<%=TextBox3.ClientID %>'>Email</label>
                                      <span class="helper-text" data-error="wrong" data-success="right">Helper text</span>
                                  </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <asp:TextBox ID="txtDate" runat="server" CssClass="datepicker"></asp:TextBox>
                                <div class="switch">
                                    <label>
                                        Off<asp:CheckBox ID="CheckBox1" runat="server" />
                                        <span class="lever"></span>
                                        On
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <br />
                <br />
                <br />
                <div class="row">
                    <div class="col s12">
                        <ul class="tabs">
                            <li class="tab col s3" runat="server" id="tab1"><a runat="server" id="a1" class="active" href="#test1">Test 1</a></li>
                            <li class="tab col s3 disabled" runat="server" id="tab2"><a runat="server" id="a2" href="#test2">Test 2</a></li>
                            <li class="tab col s3 disabled" runat="server" id="tab3"><a runat="server" id="a3" href="#test3">Disabled Tab</a></li>
                            <li class="tab col s3 disabled" runat="server" id="tab4"><a runat="server" id="a4" href="#test4">Test 4</a></li>
                        </ul>
                    </div>
                    <div id="test1" class="col s12">Test 1</div>
                    <div id="test2" class="col s12">Test 2</div>
                    <div id="test3" class="col s12">Test 3</div>
                    <div id="test4" class="col s12">Test 4</div>
                </div>


                <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
                <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                <!-- Modal Trigger -->
                <a class="waves-effect waves-light btn modal-trigger" href="#modal1">Modal</a>

                <!-- Modal Structure -->
                <div id="modal1" class="modal">
                    <div class="modal-content">
                        <h4>Modal Header</h4>
                        <p>A bunch of text</p>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="Button2" runat="server" Text="Button" OnClick="Button2_Click"/>
                        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Agree</a>
                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
        <div>
        </div>
        <script>
            function pageLoad(sender, args) {
                $(document).ready(function () {
                    $('.sidenav').sidenav();
                    $(".dropdown-trigger").dropdown({
                        closeOnClick: true
                    });
                    $('.collapsible').collapsible({ accordion: true });

                    var minDate = new Date();
                    minDate.setMonth(minDate.getMonth() - 1);

                    $('.datepicker').datepicker({
                        autoClose: true,
                        format: 'dd/mm/yyyy',
                        defaultDate: new Date(),
                        setDefaultDate: true,
                        minDate: minDate
                    });

                    $('.tabs').tabs();

                    $('.modal').modal();

                    M.updateTextFields();
                });
            }
        </script>
    </form>
</body>
</html>
