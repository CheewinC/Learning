﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Learning.Pages
{
    public partial class LoadImageFromURL : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SaveImage("https://profile.line-scdn.net/0m010684497251ed62cd6807da61e145a305124ce8d90a", @"E:\test.jfif");
            byte[] a = SaveImageToByte("https://profile.line-scdn.net/0m010684497251ed62cd6807da61e145a305124ce8d90a");
            Image1.ImageUrl = QRCodeGetBase64ImageString(a);
        }

        protected void SaveImage(string url,string filename)
        {
            using (WebClient client = new WebClient())
            {
                client.DownloadFile(new Uri(url), filename);
            }
        }

        protected byte[] SaveImageToByte(string url)
        {
            using (WebClient client = new WebClient())
            {
                return client.DownloadData(url);
            }
        }

        public static string QRCodeGetBase64ImageString(byte[] byteImage)
        {
            return "data:image/jfif;base64," + Convert.ToBase64String(byteImage);
        }
    }
}