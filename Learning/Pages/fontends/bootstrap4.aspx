﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="bootstrap4.aspx.cs" Inherits="Learning.Pages.fontends.bootstrap4" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <%--    <link href="../../Content/bootstrap.min.css" rel="stylesheet" />--%>
    <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet" />
    <link href="../../Content/customs/custom.css" rel="stylesheet" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div id="hero">
            <div class="container">
                <div class="row">
                    <nav class="navbar navbar-expand-md navbar-dark">

                        <a href="#" class="navbar-brand text-primary" id="logo">GAMELOGO</a>

                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportContent" aria-controls="navbarSupportContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-end" id="navbarSupportContent">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link text-primary" href="#">Overview</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-primary" href="#">Specs</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-primary" href="#">FAQ</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-primary" href="#">Purchase</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>

        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/jquery-3.4.1.slim.min.js" />
                <asp:ScriptReference Path="~/Scripts/popper.min.js" />
                <asp:ScriptReference Path="~/Scripts/bootstrap.min.js" />
                <asp:ScriptReference Path="~/OwlCarousel2-2.3.4/dist/owl.carousel.min.js" />
            </Scripts>
        </asp:ScriptManager>
    </form>
</body>
</html>
