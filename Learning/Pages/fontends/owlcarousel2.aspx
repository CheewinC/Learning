﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="owlcarousel2.aspx.cs" Inherits="Learning.Pages.fontends.owlcarousel2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="../../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../../OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css" rel="stylesheet" />
    <link href="../../OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css" rel="stylesheet" />
    <style>
        .owl-dots {
            position: absolute;
            bottom: 14%;
            right: 20%;
        }
        .owl-dot span{
            background-color:lightblue !important;
        }

        .owl-dot.active  span{
            background-color:aqua !important
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/jquery-3.4.1.slim.min.js" />
                <asp:ScriptReference Path="~/Scripts/popper.min.js" />
                <asp:ScriptReference Path="~/Scripts/bootstrap.min.js" />
                <asp:ScriptReference Path="~/OwlCarousel2-2.3.4/dist/owl.carousel.min.js" />
            </Scripts>
        </asp:ScriptManager>
        <div class="container">
            <div class="owl-carousel owl-theme">
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="..." alt="Card image cap" />
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="..." alt="Card image cap" />
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="..." alt="Card image cap" />
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="..." alt="Card image cap" />
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="..." alt="Card image cap" />
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="owl-demo" class="owl-carousel owl-theme">
            <div class="item">
                <img src="../../images/20200115-043624-1ecmBKcsoc.jpg" class="d-none d-sm-block" />
                <img src="../../images/20200115-043534-qaCQN8eyoD.jpg" class="d-sm-none" />
            </div>
            <div class="item">
                <img src="../../images/20200115-043624-1ecmBKcsoc.jpg" class="d-none d-sm-block" />
                <img src="../../images/20200115-043534-qaCQN8eyoD.jpg" class="d-sm-none" />
            </div>
            <div class="item">
                <img src="../../images/20200115-043624-1ecmBKcsoc.jpg" class="d-none d-sm-block" />
                <img src="../../images/20200115-043534-qaCQN8eyoD.jpg" class="d-sm-none" />
            </div>
            <div class="item">
                <img src="../../images/20200115-043624-1ecmBKcsoc.jpg" class="d-none d-sm-block" />
                <img src="../../images/20200115-043534-qaCQN8eyoD.jpg" class="d-sm-none" />
            </div>
            <div class="item">
                <img src="../../images/20200115-043624-1ecmBKcsoc.jpg" class="d-none d-sm-block" />
                <img src="../../images/20200115-043534-qaCQN8eyoD.jpg" class="d-sm-none" />
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $(".owl-carousel").owlCarousel({
                    dots: true,
                    items: 1,
                    nav: true,
                    navText: ["<button type='button' class='btn btn-info btn-sm'>Previos</button>", "<button type='button' class='btn btn-info btn-sm'>Next</button>"]
                });

                //$("#owl-demo").owlCarousel({
                //    nav: true, // Show next and prev buttons
                //    slideSpeed: 300,
                //    paginationSpeed: 400,
                //    singleItem: true

                //    // "singleItem:true" is a shortcut for:
                //    // items : 1, 
                //    // itemsDesktop : false,
                //    // itemsDesktopSmall : false,
                //    // itemsTablet: false,
                //    // itemsMobile : false
                //});
            });

        </script>
    </form>
</body>
</html>
