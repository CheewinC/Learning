﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VerticalStepper.aspx.cs" Inherits="Learning.Pages.fontends.VerticalStepper" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link href="../../Content/bootstrap.min.css" rel="stylesheet" />
    <title></title>
    <style>
        :root {
            --stepper-color: rgb(6,150,215);
        }

        html,
        body {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        .vertical-stepper .step:first-child .content {
            padding-bottom: 30px;
        }

        .step {
            /*padding: 5px;*/
            padding: 0 5px 5px 5px;
            display: flex;
            flex-direction: row;
            justify-content: flex-start;
            /*background-color: mintcream;*/
        }

        .v-stepper {
            position: relative;
        }

        /* regular step */
        .step .circle {
            background-color: white;
            border: 3px solid gray;
            border-radius: 100%;
            width: 20px;
            height: 20px;
            display: inline-block;
        }

        .step .line {
            top: 20px;
            left: 9px;
            height: 100%;
            position: absolute;
            border-left: 3px solid gray;
        }

        .step.completed .circle {
            background-color: var(--stepper-color);
            border-color: var(--stepper-color);
        }

        .step.completed .line, .step.stepActive .line {
            border-left: 3px solid var(--stepper-color);
        }

        .step.stepActive .circle {
            border-color: var(--stepper-color);
        }

        .step:last-child .line {
            border-left: 3px solid white;
            z-index: -1; /* behind the circle to completely hide */
        }

        .content {
            margin: 0 0 5px 20px;
            display: inline-block;
            width: 100%
        }

        .step i {
            font-size: 40px;
            color: rgb(103, 200, 245);
            margin: -5px 0 5px 0;
            text-align: center;
        }

        .step.completed i {
            color: var(--stepper-color);
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/jquery-3.4.1.slim.min.js" />
                <asp:ScriptReference Path="~/Scripts/popper.min.js" />
                <asp:ScriptReference Path="~/Scripts/bootstrap.min.js" />
            </Scripts>
        </asp:ScriptManager>
        <div class="container">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>First</th>
                        <th>Last</th>
                        <th>Handle</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>1</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                    </tr>
                    <tr>
                        <th>2</th>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>@fat</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td colspan="3">
                            <div class="vertical-stepper">
                                <!-- completed -->
                                <div class="step completed">
                                    <div class="v-stepper">
                                        <div class="circle"></div>
                                        <div class="line"></div>
                                    </div>

                                    <div class="content">
                                        <div class="row">
                                            <div class="col-12 col-sm-1"><i class="fas fa-money-check-alt"></i></div>
                                            <div class="col-12 col-sm">01/04/2020 11:38</div>
                                            <div class="col-12 col-sm">ส่วนวนับสนุนการขาย (สส.ตตก.)</div>
                                            <div class="col-12 col-sm">Payment Process</div>
                                        </div>
                                    </div>
                                </div>

                                <!-- active -->
                                <div class="step stepActive">
                                    <div class="v-stepper">
                                        <div class="circle"></div>
                                        <div class="line"></div>
                                    </div>

                                    <div class="content">
                                        <div class="row">
                                            <div class="col-12 col-sm-1"><i class="fas fa-search"></i></div>
                                            <div class="col-12 col-sm">01/04/2020 11:38</div>
                                            <div class="col-12 col-sm">ลำปาง</div>
                                            <div class="col-12 col-sm">Scanning Process</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="step stepActive">
                                    <div class="v-stepper">
                                        <div class="circle"></div>
                                        <div class="line"></div>
                                    </div>

                                    <div class="content">
                                        <div class="row">
                                            <div class="col-12 col-sm-1"><i class="fas fa-file-import"></i></div>
                                            <div class="col-12 col-sm">01/04/2020 11:38</div>
                                            <div class="col-12 col-sm">ส่วนจัดหาและบริหารพัสดุ (จห.บกก.)</div>
                                            <div class="col-12 col-sm">บันทึกรับเข้า</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="step stepActive">
                                    <div class="v-stepper">
                                        <div class="circle"></div>
                                        <div class="line"></div>
                                    </div>

                                    <div class="content">
                                        <div class="row">
                                            <div class="col-12 col-sm-1"><i class="fas fa-file-export"></i></div>
                                            <div class="col-12 col-sm">01/04/2020 11:38</div>
                                            <div class="col-12 col-sm">อาคาร Enco B ชั้น 16</div>
                                            <div class="col-12 col-sm">บันทึกส่งออก</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="step stepActive">
                                    <div class="v-stepper">
                                        <div class="circle"></div>
                                        <div class="line"></div>
                                    </div>

                                    <div class="content">
                                        <div class="row">
                                            <div class="col-12 col-sm-1"><i class="fas fa-address-card"></i></div>
                                            <div class="col-12 col-sm">01/04/2020 11:38</div>
                                            <div class="col-12 col-sm">ลำปาง</div>
                                            <div class="col-12 col-sm">ลงทะเบียน</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>
</body>
</html>