﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Learning.Pages.fontends
{
    public partial class amcharts : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<amchart> a = new List<amchart>
            {
                new amchart{country="USA",visits=4000},
                new amchart{country="THA",visits=2000},
                new amchart{country="THA1",visits=3000},
                new amchart{country="THA2",visits=3200},
                new amchart{country="THA3",visits=1100},
                new amchart{country="THA4",visits=1800},
                new amchart{country="THA5",visits=2300},
                new amchart{country="THA6",visits=3200},
                new amchart{country="THA7",visits=600}
            };

            string json = JsonConvert.SerializeObject(a);

            if (!IsPostBack)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "amCharts", $"amchartBar({json},'chartdiv');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "amCharts2", $"amchartBar({json},'chartdiv2');", true);
            }
        }
    }

    public class amchart
    {
        public string country { get; set; }
        public int visits { get; set; }
    }
}