﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="amcharts.aspx.cs" Inherits="Learning.Pages.fontends.amcharts" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="../../Content/bootstrap.min.css" rel="stylesheet" />
    <title></title>
    <style>
        #chartdiv {
            width: 100%;
            height: 500px;
        }

        #chartdiv2 {
            width: 100%;
            height: 500px;
        }

        #chartPieDiv {
            width: 100%;
            height: 500px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
       

        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/jquery-3.4.1.slim.min.js" />
                <asp:ScriptReference Path="~/Scripts/popper.min.js" />
                <asp:ScriptReference Path="~/Scripts/bootstrap.min.js" />
                <asp:ScriptReference Path="https://www.amcharts.com/lib/4/core.js" />
                <asp:ScriptReference Path="https://www.amcharts.com/lib/4/charts.js" />
                <asp:ScriptReference Path="https://www.amcharts.com/lib/4/themes/animated.js" />
            </Scripts>
        </asp:ScriptManager>
         <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <div id="chartdiv"></div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div id="chartdiv2"></div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <br />
                            <div id="chartPieDiv"></div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <!-- Chart code -->
        <script>
            $(document).ready(function () {
                //amchartBar();
                amchartPie(null, null);
            });

            function amchartBar(jsonStringData, elementId) {
                am4core.ready(function () {

                    // Themes begin
                    am4core.useTheme(am4themes_animated);
                    // Themes end

                    // Create chart instance
                    var chart = am4core.create(elementId, am4charts.XYChart);
                    chart.responsive.enabled = true;

                    // Title
                    var title = chart.titles.push(new am4core.Label());
                    title.text = "Coutry Visits";
                    title.fontSize = 25;
                    title.marginBottom = 15;

                    // Add data
                    chart.data = jsonStringData;
                    //chart.data = [{
                    //    "country": "USA",
                    //    "visits": 2025
                    //}, {
                    //    "country": "China",
                    //    "visits": 1882
                    //}, {
                    //    "country": "Japan",
                    //    "visits": 1809
                    //}, {
                    //    "country": "Germany",
                    //    "visits": 1322
                    //}, {
                    //    "country": "UK",
                    //    "visits": 1122
                    //}, {
                    //    "country": "France",
                    //    "visits": 1114
                    //}, {
                    //    "country": "India",
                    //    "visits": 984
                    //}, {
                    //    "country": "Spain",
                    //    "visits": 711
                    //}, {
                    //    "country": "Netherlands",
                    //    "visits": 665
                    //}, {
                    //    "country": "Russia",
                    //    "visits": 580
                    //}, {
                    //    "country": "South Korea",
                    //    "visits": 443
                    //}, {
                    //    "country": "Canada",
                    //    "visits": 441
                    //}, {
                    //    "country": "Brazil",
                    //    "visits": 395
                    //}];

                    // Create axes
                    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                    categoryAxis.title.text = "Country";
                    categoryAxis.dataFields.category = "country";
                    categoryAxis.renderer.grid.template.location = 0;
                    categoryAxis.renderer.minGridDistance = 30;

                    //categoryAxis.renderer.labels.template.adapter.add("dy", function (dy, target) {
                    //    if (target.dataItem && target.dataItem.index & 2 == 2) {
                    //        return dy + 25;
                    //    }
                    //    return dy;
                    //});

                    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                    valueAxis.title.text = "Visits (Times)";

                    // Create series
                    var series = chart.series.push(new am4charts.ColumnSeries());
                    series.dataFields.valueY = "visits";
                    series.dataFields.categoryX = "country";
                    series.name = "Visits";
                    series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
                    series.columns.template.fillOpacity = .8;

                    var columnTemplate = series.columns.template;
                    columnTemplate.strokeWidth = 2;
                    columnTemplate.strokeOpacity = 1;

                    series.columns.template.adapter.add("fill", function (fill, target) {
                        return chart.colors.getIndex(target.dataItem.index);
                    });

                    // Cursor
                    chart.cursor = new am4charts.XYCursor();
                }); // end am4core.ready()
            }

            function amchartPie(jsonStringData, elementId) {
                am4core.ready(function () {

                    // Themes begin
                    am4core.useTheme(am4themes_animated);
                    // Themes end

                    // Create chart instance
                    var chart = am4core.create("chartPieDiv", am4charts.PieChart);

                    // Responsive
                    chart.responsive.enabled = true;
                    chart.responsive.rules.push({
                        relevant: function (target) {
                            if (target.pixelWidth <= 600) {
                                return true;
                            }
                            return false;
                        },
                        state: function (target, stateId) {
                            if (target instanceof am4charts.PieSeries) {
                                var state = target.states.create(stateId);

                                var labelState = target.labels.template.states.create(stateId);
                                labelState.properties.disabled = true;

                                var tickState = target.ticks.template.states.create(stateId);
                                tickState.properties.disabled = true;
                                return state;
                            }

                            return null;
                        }
                    });
                    //1.	สถานะลงทะเบียน
                    //2.	สถานะบันทึกการส่งออก
                    //3.	สถานะบันทึกรับเข้า
                    //4.	สถานะบันทึกส่งออก
                    //5.	สถานะ Scanning Process
                    //6.	สถานะ Invoice Process
                    //7.	สถานะ Payment Process

                    // Add data
                    chart.data = [
                        { "sector": "สถานะลงทะเบียน", "size":15 },
                        { "sector": "สถานะบันทึกการส่งออก", "size": 5 },
                        { "sector": "สถานะบันทึกรับเข้า", "size": 10 },
                        { "sector": "สถานะบันทึกส่งออก", "size": 8 },
                        { "sector": "สถานะ Scanning Process", "size": 4 },
                        { "sector": "สถานะ Invoice Process", "size": 3 },
                        { "sector": "สถานะ Payment Process", "size": 2 },
                    ];

                    // Add and configure Series
                    var pieSeries = chart.series.push(new am4charts.PieSeries());
                    pieSeries.dataFields.value = "size";
                    pieSeries.dataFields.category = "sector";

                    // Add a legend
                    chart.legend = new am4charts.Legend();
                    //chart.legend.position = "right";
                    chart.legend.maxWidth = undefined;
                    chart.legend.labels.template.text = "[bold {color}]{name}[/]";
                    chart.legend.valueLabels.template.text = "[bold{color}]{value.percent.formatNumber('#.0')}% ({value})[/]";

                    // Add label
                    //chart.innerRadius = 100;
                    chart.innerRadius = am4core.percent(50);
                    var label = pieSeries.createChild(am4core.Label);
                    label.text = "{values.value.sum} รายการ";
                    label.maxWidth = 100;
                    label.wrap = true;
                    label.horizontalCenter = "middle";
                    label.verticalCenter = "middle";
                    label.fontSize = 20;

                    pieSeries.slices.template.events.on("hit", function (ev) {
                        console.log("clicked on ", ev.target.dataItem.dataContext.sector);
                    }, this);
                }); // end am4core.ready()
            }

            function myFunction(ev) {
                console.log("clicked on ", ev.target);
            }
        </script>
    </form>

</body>
</html>
