﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SweetAlert2.aspx.cs" Inherits="Learning.Pages.fontends.SweetAlert2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@sweetalert2/themes@3.1.4/bootstrap-4/bootstrap-4.min.css"/>
</head>
<body>
    <!--https://sweetalert2.github.io/-->
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/jquery-3.4.1.slim.min.js" />
                <asp:ScriptReference Path="~/Scripts/popper.min.js" />
                <asp:ScriptReference Path="~/Scripts/bootstrap.min.js" />
                <asp:ScriptReference Path="https://cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js" />
            </Scripts>
        </asp:ScriptManager>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <button id="sw">sweet</button>
            </ContentTemplate>
        </asp:UpdatePanel>
        <script>
            $(function () {
                $(document).on('click', '#sw', function () {
                    //$('#sw').click(function () {
                    //sweetAlert('success', '<b style="color:green">ชำระเงินสำเร็จ</b>', 'ระบบกำลังสร้างกรมธรรม์ กรุณารอสักครู่ <br/> หากใช้เวลานานกว่า 5 นาที กรุณาติดต่อแอดมิน');
                    sweetAlertHtml('success', '<b style="color:green">ชำระเงินสำเร็จ</b>', '<span style="font-size:0.9rem">ระบบกำลังสร้างกรมธรรม์ กรุณารอสักครู่... <br/> หากท่านไม่ได้รับกรมธรรม์ภายในเวลา 5 นาที กรุณาติดต่อแอดมิน<span>');
                    sweetAlertHtml('error', '<b style="color:red">ชำระเงินไม่สำเร็จ</b>', '<span style="font-size:0.9rem">กรุณากลับไปชำระเงินใหม่<br/> หรือหากท่านมีปัญหาในการชำระเงิน กรุณาติดต่อแอดมิน<span>');
                });
            });

            function sweetAlert2() {
                Swal.fire({
                    title: 'Error!',
                    text: 'Do you want to continue',
                    icon: 'error',
                    confirmButtonText: 'Cool'
                });
            }
            function sweetAlert(_icon,_title,_text,) {
                Swal.fire({
                    icon: _icon,
                    title: _title,
                    text: _text,
                    showCloseButton: true,
                })
            }

            function sweetAlertHtml(_icon, _title, _text, ) {
                Swal.fire({
                    icon: _icon,
                    title: _title,
                    html: _text,
                    showCloseButton: true,
                })
            }

        </script>
    </form>

</body>
</html>
