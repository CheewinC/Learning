﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Learning.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Learning.Pages
{
    public partial class CrystalReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string ass = HttpUtility.UrlEncode("ABCD");
            DataContext db = new DataContext();
            //db.Persons.Add(new Person { Id = Guid.NewGuid(), Name = "Person1",Picture = @"C:\Users\NecC\Downloads\78219739_1393102127516818_5402188682130096128_n.jpg" });
            //db.Persons.Add(new Person { Id = Guid.NewGuid(), Name = "Person1", Picture = @"C:\Users\NecC\Downloads\78337400_516834912374377_213150752930004992_n.jpg" });
            //db.SaveChanges();
            var dc = db.Persons.ToList();

            CrystalReport1 rd = new CrystalReport1();
            rd.SetDataSource(dc);
            TextObject name = rd.ReportDefinition.Sections[1].ReportObjects["Name"] as TextObject;
            PictureObject logo = rd.Section1.ReportObjects["logo"] as PictureObject;
            logo.Width = 500;
            logo.Height = 500;
            name.Text = "Cheewin";
            rd.SetParameterValue("paraPicture", @"C:\Users\NecC\Downloads\maintenance-2422173_1920.png");
            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.DataBind();
            rd.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, false,"Test");
        }
    }
}