﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DatePicker.aspx.cs" Inherits="Learning.Pages.DatePicker" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <%--<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />--%>
    <link href="../Content/bootstrap-datepicker.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/jquery-3.4.1.slim.min.js" />
                <asp:ScriptReference Path="~/Scripts/popper.min.js" />
                <asp:ScriptReference Path="~/Scripts/bootstrap.min.js" />
                <asp:ScriptReference Path="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" />
                <asp:ScriptReference Path="~/Scripts/bootstrap-datepicker.js" />
            </Scripts>
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" TextMode="Date"></asp:TextBox>
                        </div>
                        <br />
                        <div class="col-12">
                            <asp:TextBox ID="txtDatePicker" runat="server" CssClass="datepicker form-control" AutoCompleteType="Disabled"></asp:TextBox>
                            <input type='text' class="datepicker form-control" id="uxDateTimeLocalTextbox" value="" runat="server" ClientIDMode="Static" />
                        </div>
                    </div>
                </div>
                <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
                <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
        <script>
            function pageLoad(sender, args) {
                $(document).ready(function () {
                    $('.datepicker').datepicker({
                        //uiLibrary: 'bootstrap4'
                        format: 'dd/mm/yyyy'
                        ,
                        autoclose: true
                    });
                });
            }
           
        </script>
    </form>
</body>
</html>
