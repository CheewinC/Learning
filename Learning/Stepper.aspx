﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Stepper.aspx.cs" Inherits="Learning.Stepper" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <%--<style>
        /*--------------------start progress bar---------------------------*/
        .card-progress {
            z-index: -2;
        }

            .card-progress .card-body {
                padding-right: 0;
                padding-left: 0;
            }

        .progressbar {
            margin: 0;
            padding: 0;
            counter-reset: step;
        }

            .progressbar li {
                list-style-type: none;
                width: 20%;
                float: left;
                font-size: 12px;
                position: relative;
                text-align: center;
                text-transform: uppercase;
                color: #7d7d7d;
            }

                .progressbar li:before {
                    width: 30px;
                    height: 30px;
                    content: counter(step);
                    counter-increment: step;
                    line-height: 30px;
                    border: 2px solid #7d7d7d;
                    display: block;
                    text-align: center;
                    margin: 0 auto 10px auto;
                    border-radius: 50%;
                    background-color: white;
                }

                .progressbar li:after {
                    width: 100%;
                    height: 2px;
                    content: '';
                    position: absolute;
                    background-color: #7d7d7d;
                    top: 15px;
                    left: -50%;
                    z-index: -1;
                }

                .progressbar li:first-child:after {
                    content: none;
                }

                .progressbar li.active, .progressbar li.current {
                    color: #0077c2;
                }

                    .progressbar li.active:before {
                        color: white;
                        border-color: #0077c2;
                        background-color: #0077c2;
                    }

                    .progressbar li.active + li:after {
                        background-color: #0077c2;
                    }

                    .progressbar li.current:before {
                        border-color: #0077c2;
                        color: #0077c2;
                    }
        /*--------------------end progress bar---------------------------*/
    </style>--%>
    <style>
        /*--------------------start progress bar---------------------------*/
        .progressbar {
            margin: 0;
            padding: 0;
            counter-reset: step;
        }

            .progressbar li {
                list-style-type: none;
                width: 20%;
                float: left;
                font-size: 12px;
                position: relative;
                text-align: center;
                text-transform: uppercase;
                color: #7d7d7d;
            }

            .progressbar span:before {
                margin-top: 5px !important;
                width: 20px;
                height: 20px;
                content: counter(step);
                counter-increment: step;
                line-height: 20px;
                border: 2px solid #7d7d7d;
                display: block;
                text-align: center;
                margin: 0 auto 10px auto;
                border-radius: 50%;
                background-color: white;
            }

            .progressbar li:after {
                width: 100%;
                height: 2px;
                content: '';
                position: absolute;
                background-color: #7d7d7d;
                top: 66px;
                left: -50%;
                z-index: -1;
            }

            .progressbar li:first-child:after {
                content: none;
            }

            .progressbar li.active, .progressbar li.current {
                color: #0077c2;
            }

                .progressbar li.active span:before {
                    color: white;
                    border-color: #0077c2;
                    background-color: #0077c2;
                }

                .progressbar li.active + li:after {
                    background-color: #0077c2;
                }

                .progressbar li.current span:before {
                    border-color: #0077c2;
                    color: #0077c2;
                }
        /*--------------------end progress bar---------------------------*/
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>Icons made by <a href="https://www.flaticon.com/authors/those-icons" title="Those Icons">Those Icons</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
        <div>
            <ul class="progressbar">
                <li class="active">
                    <%--<img src="images/note.svg" style="width: 50px; fill: red" />--%>
                    <img src="images/note.svg" style="width: 50px" />
                    <%--<i class="fas fa-address-card" style="font-size: 50px"></i>--%>
                    <span></span>
                    <p>ลงทะเบียน</p>
                </li>
                <li class="current">
                    <i class="fas fa-file-export" style="font-size: 50px"></i>
                    <span></span>
                    <p>บันทึกส่งออก</p>
                </li>
                <li>
                    <i class="fas fa-file-import" style="font-size: 50px"></i>
                    <span></span>
                    <p>บันทึกรับเข้า</p>
                </li>
                <li>
                    <i class="fas fa-search" style="font-size: 50px"></i>
                    <span></span>
                    <p>Scanning Process</p>
                </li>
                <li>
                    <i class="fas fa-money-check-alt" style="font-size: 50px"></i>
                    <span></span>
                    <p>Payment Process</p>
                </li>
            </ul>
        </div>
    </form>
</body>
</html>