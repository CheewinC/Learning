func @_Microsoft.AspNet.SignalR.StockTicker.StockTicker.GetAllStocks$$() -> none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :55 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :57 :19) // Not a variable of known type: _stocks
%1 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :57 :19) // _stocks.Values (SimpleMemberAccessExpression)
return %1 : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :57 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Microsoft.AspNet.SignalR.StockTicker.StockTicker.OpenMarket$$() -> () loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :60 :8) {
^entry :
br ^0

^0: // LockBlock
%0 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :62 :18) // Not a variable of known type: _marketStateLock
br ^1

^1: // BinaryBranchBlock
%1 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :64 :20) // Not a variable of known type: MarketState
%2 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :64 :35) // Not a variable of known type: MarketState
%3 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :64 :35) // MarketState.Open (SimpleMemberAccessExpression)
%4 = cbde.unknown : i1  loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :64 :20) // comparison of unknown type: MarketState != MarketState.Open
cond_br %4, ^2, ^3 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :64 :20)

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: UpdateStockPrices
%5 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :66 :58) // null (NullLiteralExpression)
%6 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :66 :64) // Not a variable of known type: _updateInterval
%7 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :66 :81) // Not a variable of known type: _updateInterval
%8 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :66 :29) // new Timer(UpdateStockPrices, null, _updateInterval, _updateInterval) (ObjectCreationExpression)
%9 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :68 :34) // Not a variable of known type: MarketState
%10 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :68 :34) // MarketState.Open (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: BroadcastMarketStateChange
%11 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :70 :47) // Not a variable of known type: MarketState
%12 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :70 :47) // MarketState.Open (SimpleMemberAccessExpression)
%13 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :70 :20) // BroadcastMarketStateChange(MarketState.Open) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Microsoft.AspNet.SignalR.StockTicker.StockTicker.CloseMarket$$() -> () loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :75 :8) {
^entry :
br ^0

^0: // LockBlock
%0 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :77 :18) // Not a variable of known type: _marketStateLock
br ^1

^1: // BinaryBranchBlock
%1 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :79 :20) // Not a variable of known type: MarketState
%2 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :79 :35) // Not a variable of known type: MarketState
%3 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :79 :35) // MarketState.Open (SimpleMemberAccessExpression)
%4 = cbde.unknown : i1  loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :79 :20) // comparison of unknown type: MarketState == MarketState.Open
cond_br %4, ^2, ^3 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :79 :20)

^2: // BinaryBranchBlock
%5 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :81 :24) // Not a variable of known type: _timer
%6 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :81 :34) // null (NullLiteralExpression)
%7 = cbde.unknown : i1  loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :81 :24) // comparison of unknown type: _timer != null
cond_br %7, ^4, ^5 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :81 :24)

^4: // SimpleBlock
%8 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :83 :24) // Not a variable of known type: _timer
%9 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :83 :24) // _timer.Dispose() (InvocationExpression)
br ^5

^5: // SimpleBlock
%10 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :86 :34) // Not a variable of known type: MarketState
%11 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :86 :34) // MarketState.Closed (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: BroadcastMarketStateChange
%12 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :88 :47) // Not a variable of known type: MarketState
%13 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :88 :47) // MarketState.Closed (SimpleMemberAccessExpression)
%14 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :88 :20) // BroadcastMarketStateChange(MarketState.Closed) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Microsoft.AspNet.SignalR.StockTicker.StockTicker.Reset$$() -> () loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :93 :8) {
^entry :
br ^0

^0: // LockBlock
%0 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :95 :18) // Not a variable of known type: _marketStateLock
br ^1

^1: // BinaryBranchBlock
%1 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :97 :20) // Not a variable of known type: MarketState
%2 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :97 :35) // Not a variable of known type: MarketState
%3 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :97 :35) // MarketState.Closed (SimpleMemberAccessExpression)
%4 = cbde.unknown : i1  loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :97 :20) // comparison of unknown type: MarketState != MarketState.Closed
cond_br %4, ^2, ^3 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :97 :20)

^2: // JumpBlock
%5 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :99 :56) // "Market must be closed before it can be reset." (StringLiteralExpression)
%6 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :99 :26) // new InvalidOperationException("Market must be closed before it can be reset.") (ObjectCreationExpression)
cbde.throw %6 :  none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :99 :20)

^3: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: LoadDefaultStocks
%7 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :102 :16) // LoadDefaultStocks() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: BroadcastMarketReset
%8 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :103 :16) // BroadcastMarketReset() (InvocationExpression)
br ^4

^4: // ExitBlock
return

}
// Skipping function LoadDefaultStocks(), it contains poisonous unsupported syntaxes

// Skipping function UpdateStockPrices(none), it contains poisonous unsupported syntaxes

// Skipping function TryUpdateStockPrice(none), it contains poisonous unsupported syntaxes

// Skipping function BroadcastMarketStateChange(none), it contains poisonous unsupported syntaxes

func @_Microsoft.AspNet.SignalR.StockTicker.StockTicker.BroadcastMarketReset$$() -> () loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :178 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :180 :12) // Not a variable of known type: Clients
%1 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :180 :12) // Clients.All (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :180 :12) // Clients.All.marketReset (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :180 :12) // Clients.All.marketReset() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Microsoft.AspNet.SignalR.StockTicker.StockTicker.BroadcastStockPrice$Microsoft.AspNet.SignalR.StockTicker.Stock$(none) -> () loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :183 :8) {
^entry (%_stock : none):
%0 = cbde.alloca none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :183 :41)
cbde.store %_stock, %0 : memref<none> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :183 :41)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :185 :12) // Not a variable of known type: Clients
%2 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :185 :12) // Clients.All (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :185 :12) // Clients.All.updateStockPrice (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :185 :41) // Not a variable of known type: stock
%5 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\SignalRSample\\SignalR.Sample\\StockTicker.cs" :185 :12) // Clients.All.updateStockPrice(stock) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
