func @_Learning.SoapDemo.SoapDemoSoapClient.AdditionInt$int.int$(i32, i32) -> i32 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\Learning\\Connected Services\\SoapDemo\\Reference.cs" :51 :8) {
^entry (%_num1 : i32, %_num2 : i32):
%0 = cbde.alloca i32 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\Learning\\Connected Services\\SoapDemo\\Reference.cs" :51 :31)
cbde.store %_num1, %0 : memref<i32> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\Learning\\Connected Services\\SoapDemo\\Reference.cs" :51 :31)
%1 = cbde.alloca i32 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\Learning\\Connected Services\\SoapDemo\\Reference.cs" :51 :41)
cbde.store %_num2, %1 : memref<i32> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\Learning\\Connected Services\\SoapDemo\\Reference.cs" :51 :41)
br ^0

^0: // JumpBlock
%2 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\Learning\\Connected Services\\SoapDemo\\Reference.cs" :52 :19) // base (BaseExpression)
%3 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\Learning\\Connected Services\\SoapDemo\\Reference.cs" :52 :19) // base.Channel (SimpleMemberAccessExpression)
%4 = cbde.load %0 : memref<i32> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\Learning\\Connected Services\\SoapDemo\\Reference.cs" :52 :44)
%5 = cbde.load %1 : memref<i32> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\Learning\\Connected Services\\SoapDemo\\Reference.cs" :52 :50)
%6 = cbde.unknown : i32 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\Learning\\Connected Services\\SoapDemo\\Reference.cs" :52 :19) // base.Channel.AdditionInt(num1, num2) (InvocationExpression)
return %6 : i32 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\Learning\\Connected Services\\SoapDemo\\Reference.cs" :52 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Learning.SoapDemo.SoapDemoSoapClient.AdditionIntAsync$int.int$(i32, i32) -> none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\Learning\\Connected Services\\SoapDemo\\Reference.cs" :55 :8) {
^entry (%_num1 : i32, %_num2 : i32):
%0 = cbde.alloca i32 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\Learning\\Connected Services\\SoapDemo\\Reference.cs" :55 :65)
cbde.store %_num1, %0 : memref<i32> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\Learning\\Connected Services\\SoapDemo\\Reference.cs" :55 :65)
%1 = cbde.alloca i32 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\Learning\\Connected Services\\SoapDemo\\Reference.cs" :55 :75)
cbde.store %_num2, %1 : memref<i32> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\Learning\\Connected Services\\SoapDemo\\Reference.cs" :55 :75)
br ^0

^0: // JumpBlock
%2 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\Learning\\Connected Services\\SoapDemo\\Reference.cs" :56 :19) // base (BaseExpression)
%3 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\Learning\\Connected Services\\SoapDemo\\Reference.cs" :56 :19) // base.Channel (SimpleMemberAccessExpression)
%4 = cbde.load %0 : memref<i32> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\Learning\\Connected Services\\SoapDemo\\Reference.cs" :56 :49)
%5 = cbde.load %1 : memref<i32> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\Learning\\Connected Services\\SoapDemo\\Reference.cs" :56 :55)
%6 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\Learning\\Connected Services\\SoapDemo\\Reference.cs" :56 :19) // base.Channel.AdditionIntAsync(num1, num2) (InvocationExpression)
return %6 : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\Learning\\Connected Services\\SoapDemo\\Reference.cs" :56 :12)

^1: // ExitBlock
cbde.unreachable

}
