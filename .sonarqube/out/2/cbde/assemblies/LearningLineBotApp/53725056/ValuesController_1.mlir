func @_LearningLineBotApp.Controllers.ValuesController.Get$$() -> none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\LearningLineBotApp\\Controllers\\ValuesController.cs" :23 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\LearningLineBotApp\\Controllers\\ValuesController.cs" :25 :30) //  (OmittedArraySizeExpression)
%1 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\LearningLineBotApp\\Controllers\\ValuesController.cs" :25 :23) // string[] (ArrayType)
%2 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\LearningLineBotApp\\Controllers\\ValuesController.cs" :25 :19) // new string[] { "value1", "value2" } (ArrayCreationExpression)
%3 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\LearningLineBotApp\\Controllers\\ValuesController.cs" :25 :34) // "value1" (StringLiteralExpression)
%4 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\LearningLineBotApp\\Controllers\\ValuesController.cs" :25 :44) // "value2" (StringLiteralExpression)
return %2 : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\LearningLineBotApp\\Controllers\\ValuesController.cs" :25 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_LearningLineBotApp.Controllers.ValuesController.Get$int$(i32) -> none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\LearningLineBotApp\\Controllers\\ValuesController.cs" :29 :8) {
^entry (%_id : i32):
%0 = cbde.alloca i32 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\LearningLineBotApp\\Controllers\\ValuesController.cs" :29 :26)
cbde.store %_id, %0 : memref<i32> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\LearningLineBotApp\\Controllers\\ValuesController.cs" :29 :26)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\LearningLineBotApp\\Controllers\\ValuesController.cs" :31 :19) // "value" (StringLiteralExpression)
return %1 : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\LearningLineBotApp\\Controllers\\ValuesController.cs" :31 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function PostAsync(none), it contains poisonous unsupported syntaxes

func @_LearningLineBotApp.Controllers.ValuesController.Put$int.string$(i32, none) -> () loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\LearningLineBotApp\\Controllers\\ValuesController.cs" :56 :8) {
^entry (%_id : i32, %_value : none):
%0 = cbde.alloca i32 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\LearningLineBotApp\\Controllers\\ValuesController.cs" :56 :24)
cbde.store %_id, %0 : memref<i32> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\LearningLineBotApp\\Controllers\\ValuesController.cs" :56 :24)
%1 = cbde.alloca none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\LearningLineBotApp\\Controllers\\ValuesController.cs" :56 :32)
cbde.store %_value, %1 : memref<none> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\LearningLineBotApp\\Controllers\\ValuesController.cs" :56 :32)
br ^0

^0: // ExitBlock
return

}
func @_LearningLineBotApp.Controllers.ValuesController.Delete$int$(i32) -> () loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\LearningLineBotApp\\Controllers\\ValuesController.cs" :61 :8) {
^entry (%_id : i32):
%0 = cbde.alloca i32 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\LearningLineBotApp\\Controllers\\ValuesController.cs" :61 :27)
cbde.store %_id, %0 : memref<i32> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\LearningLineBotApp\\Controllers\\ValuesController.cs" :61 :27)
br ^0

^0: // ExitBlock
return

}
