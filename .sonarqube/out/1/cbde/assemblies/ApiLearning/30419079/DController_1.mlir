func @_ApiLearning.Controllers.DController.GetD$$() -> none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :19 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :21 :19) // Not a variable of known type: db
%1 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :21 :19) // db.D (SimpleMemberAccessExpression)
return %1 : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :21 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_ApiLearning.Controllers.DController.GetD$int$(i32) -> none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :25 :8) {
^entry (%_id : i32):
%0 = cbde.alloca i32 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :26 :38)
cbde.store %_id, %0 : memref<i32> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :26 :38)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :28 :18) // Not a variable of known type: db
%2 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :28 :18) // db.D (SimpleMemberAccessExpression)
%3 = cbde.load %0 : memref<i32> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :28 :28)
%4 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :28 :18) // db.D.Find(id) (InvocationExpression)
%6 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :29 :16) // Not a variable of known type: d
%7 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :29 :21) // null (NullLiteralExpression)
%8 = cbde.unknown : i1  loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :29 :16) // comparison of unknown type: d == null
cond_br %8, ^1, ^2 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :29 :16)

^1: // JumpBlock
// Entity from another assembly: NotFound
%9 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :31 :23) // NotFound() (InvocationExpression)
return %9 : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :31 :16)

^2: // JumpBlock
// Entity from another assembly: Ok
%10 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :34 :22) // Not a variable of known type: d
%11 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :34 :19) // Ok(d) (InvocationExpression)
return %11 : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :34 :12)

^3: // ExitBlock
cbde.unreachable

}
// Skipping function PutD(i32, none), it contains poisonous unsupported syntaxes

func @_ApiLearning.Controllers.DController.PostD$ApiLearning.Models.D$(none) -> none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :73 :8) {
^entry (%_d : none):
%0 = cbde.alloca none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :74 :39)
cbde.store %_d, %0 : memref<none> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :74 :39)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :76 :17) // Identifier from another assembly: ModelState
%2 = cbde.unknown : i1 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :76 :17) // ModelState.IsValid (SimpleMemberAccessExpression)
%3 = cbde.unknown : i1 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :76 :16) // !ModelState.IsValid (LogicalNotExpression)
cond_br %3, ^1, ^2 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :76 :16)

^1: // JumpBlock
// Entity from another assembly: BadRequest
%4 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :78 :34) // Identifier from another assembly: ModelState
%5 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :78 :23) // BadRequest(ModelState) (InvocationExpression)
return %5 : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :78 :16)

^2: // JumpBlock
%6 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :81 :12) // Not a variable of known type: db
%7 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :81 :12) // db.D (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :81 :21) // Not a variable of known type: d
%9 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :81 :12) // db.D.Add(d) (InvocationExpression)
%10 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :82 :12) // Not a variable of known type: db
%11 = cbde.unknown : i32 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :82 :12) // db.SaveChanges() (InvocationExpression)
// Entity from another assembly: CreatedAtRoute
%12 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :84 :34) // "DefaultApi" (StringLiteralExpression)
%13 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :84 :59) // Not a variable of known type: d
%14 = cbde.unknown : i32 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :84 :59) // d.Id (SimpleMemberAccessExpression)
%15 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :84 :48) // new { id = d.Id } (AnonymousObjectCreationExpression)
%16 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :84 :67) // Not a variable of known type: d
%17 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :84 :19) // CreatedAtRoute("DefaultApi", new { id = d.Id }, d) (InvocationExpression)
return %17 : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :84 :12)

^3: // ExitBlock
cbde.unreachable

}
func @_ApiLearning.Controllers.DController.DeleteD$int$(i32) -> none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :88 :8) {
^entry (%_id : i32):
%0 = cbde.alloca i32 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :89 :41)
cbde.store %_id, %0 : memref<i32> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :89 :41)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :91 :18) // Not a variable of known type: db
%2 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :91 :18) // db.D (SimpleMemberAccessExpression)
%3 = cbde.load %0 : memref<i32> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :91 :28)
%4 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :91 :18) // db.D.Find(id) (InvocationExpression)
%6 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :92 :16) // Not a variable of known type: d
%7 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :92 :21) // null (NullLiteralExpression)
%8 = cbde.unknown : i1  loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :92 :16) // comparison of unknown type: d == null
cond_br %8, ^1, ^2 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :92 :16)

^1: // JumpBlock
// Entity from another assembly: NotFound
%9 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :94 :23) // NotFound() (InvocationExpression)
return %9 : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :94 :16)

^2: // JumpBlock
%10 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :97 :12) // Not a variable of known type: db
%11 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :97 :12) // db.D (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :97 :24) // Not a variable of known type: d
%13 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :97 :12) // db.D.Remove(d) (InvocationExpression)
%14 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :98 :12) // Not a variable of known type: db
%15 = cbde.unknown : i32 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :98 :12) // db.SaveChanges() (InvocationExpression)
// Entity from another assembly: Ok
%16 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :100 :22) // Not a variable of known type: d
%17 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :100 :19) // Ok(d) (InvocationExpression)
return %17 : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :100 :12)

^3: // ExitBlock
cbde.unreachable

}
func @_ApiLearning.Controllers.DController.Dispose$bool$(i1) -> () loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :103 :8) {
^entry (%_disposing : i1):
%0 = cbde.alloca i1 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :103 :40)
cbde.store %_disposing, %0 : memref<i1> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :103 :40)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.load %0 : memref<i1> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :105 :16)
cond_br %1, ^1, ^2 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :105 :16)

^1: // SimpleBlock
%2 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :107 :16) // Not a variable of known type: db
%3 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :107 :16) // db.Dispose() (InvocationExpression)
br ^2

^2: // SimpleBlock
%4 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :109 :12) // base (BaseExpression)
%5 = cbde.load %0 : memref<i1> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :109 :25)
%6 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\DController.cs" :109 :12) // base.Dispose(disposing) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function DExists(i32), it contains poisonous unsupported syntaxes

