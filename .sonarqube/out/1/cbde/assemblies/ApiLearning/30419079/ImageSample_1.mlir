// Skipping function Equals(none), it contains poisonous unsupported syntaxes

func @_ApiLearning.Areas.HelpPage.ImageSample.GetHashCode$$() -> i32 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Areas\\HelpPage\\SampleGeneration\\ImageSample.cs" :30 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Areas\\HelpPage\\SampleGeneration\\ImageSample.cs" :32 :19) // Not a variable of known type: Src
%1 = cbde.unknown : i32 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Areas\\HelpPage\\SampleGeneration\\ImageSample.cs" :32 :19) // Src.GetHashCode() (InvocationExpression)
return %1 : i32 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Areas\\HelpPage\\SampleGeneration\\ImageSample.cs" :32 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_ApiLearning.Areas.HelpPage.ImageSample.ToString$$() -> none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Areas\\HelpPage\\SampleGeneration\\ImageSample.cs" :35 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Areas\\HelpPage\\SampleGeneration\\ImageSample.cs" :37 :19) // Not a variable of known type: Src
return %0 : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Areas\\HelpPage\\SampleGeneration\\ImageSample.cs" :37 :12)

^1: // ExitBlock
cbde.unreachable

}
