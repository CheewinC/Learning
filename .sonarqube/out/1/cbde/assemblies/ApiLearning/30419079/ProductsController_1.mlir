func @_ApiLearning.Controllers.ProductsController.Get$$() -> none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ProductsController.cs" :14 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ProductsController.cs" :16 :27) // Not a variable of known type: db
%1 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ProductsController.cs" :16 :27) // db.Products (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ProductsController.cs" :17 :19) // Not a variable of known type: products
return %3 : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ProductsController.cs" :17 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function Get(i32), it contains poisonous unsupported syntaxes

// Skipping function Post(none), it contains poisonous unsupported syntaxes

func @_ApiLearning.Controllers.ProductsController.Put$int.string$(i32, none) -> () loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ProductsController.cs" :50 :8) {
^entry (%_id : i32, %_value : none):
%0 = cbde.alloca i32 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ProductsController.cs" :50 :24)
cbde.store %_id, %0 : memref<i32> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ProductsController.cs" :50 :24)
%1 = cbde.alloca none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ProductsController.cs" :50 :32)
cbde.store %_value, %1 : memref<none> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ProductsController.cs" :50 :32)
br ^0

^0: // ExitBlock
return

}
func @_ApiLearning.Controllers.ProductsController.Delete$int$(i32) -> () loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ProductsController.cs" :55 :8) {
^entry (%_id : i32):
%0 = cbde.alloca i32 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ProductsController.cs" :55 :27)
cbde.store %_id, %0 : memref<i32> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ProductsController.cs" :55 :27)
br ^0

^0: // ExitBlock
return

}
