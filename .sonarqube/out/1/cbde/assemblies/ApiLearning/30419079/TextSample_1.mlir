// Skipping function Equals(none), it contains poisonous unsupported syntaxes

func @_ApiLearning.Areas.HelpPage.TextSample.GetHashCode$$() -> i32 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Areas\\HelpPage\\SampleGeneration\\TextSample.cs" :26 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Areas\\HelpPage\\SampleGeneration\\TextSample.cs" :28 :19) // Not a variable of known type: Text
%1 = cbde.unknown : i32 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Areas\\HelpPage\\SampleGeneration\\TextSample.cs" :28 :19) // Text.GetHashCode() (InvocationExpression)
return %1 : i32 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Areas\\HelpPage\\SampleGeneration\\TextSample.cs" :28 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_ApiLearning.Areas.HelpPage.TextSample.ToString$$() -> none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Areas\\HelpPage\\SampleGeneration\\TextSample.cs" :31 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Areas\\HelpPage\\SampleGeneration\\TextSample.cs" :33 :19) // Not a variable of known type: Text
return %0 : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Areas\\HelpPage\\SampleGeneration\\TextSample.cs" :33 :12)

^1: // ExitBlock
cbde.unreachable

}
