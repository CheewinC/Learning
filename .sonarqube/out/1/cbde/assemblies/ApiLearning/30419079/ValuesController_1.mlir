func @_ApiLearning.Controllers.ValuesController.Get$$() -> none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ValuesController.cs" :12 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ValuesController.cs" :14 :30) //  (OmittedArraySizeExpression)
%1 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ValuesController.cs" :14 :23) // string[] (ArrayType)
%2 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ValuesController.cs" :14 :19) // new string[] { "value1", "value2" } (ArrayCreationExpression)
%3 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ValuesController.cs" :14 :34) // "value1" (StringLiteralExpression)
%4 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ValuesController.cs" :14 :44) // "value2" (StringLiteralExpression)
return %2 : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ValuesController.cs" :14 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_ApiLearning.Controllers.ValuesController.Get$int$(i32) -> none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ValuesController.cs" :18 :8) {
^entry (%_id : i32):
%0 = cbde.alloca i32 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ValuesController.cs" :18 :26)
cbde.store %_id, %0 : memref<i32> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ValuesController.cs" :18 :26)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ValuesController.cs" :20 :19) // "value" (StringLiteralExpression)
return %1 : none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ValuesController.cs" :20 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_ApiLearning.Controllers.ValuesController.Post$string$(none) -> () loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ValuesController.cs" :24 :8) {
^entry (%_value : none):
%0 = cbde.alloca none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ValuesController.cs" :24 :25)
cbde.store %_value, %0 : memref<none> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ValuesController.cs" :24 :25)
br ^0

^0: // ExitBlock
return

}
func @_ApiLearning.Controllers.ValuesController.Put$int.string$(i32, none) -> () loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ValuesController.cs" :29 :8) {
^entry (%_id : i32, %_value : none):
%0 = cbde.alloca i32 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ValuesController.cs" :29 :24)
cbde.store %_id, %0 : memref<i32> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ValuesController.cs" :29 :24)
%1 = cbde.alloca none loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ValuesController.cs" :29 :32)
cbde.store %_value, %1 : memref<none> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ValuesController.cs" :29 :32)
br ^0

^0: // ExitBlock
return

}
func @_ApiLearning.Controllers.ValuesController.Delete$int$(i32) -> () loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ValuesController.cs" :34 :8) {
^entry (%_id : i32):
%0 = cbde.alloca i32 loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ValuesController.cs" :34 :27)
cbde.store %_id, %0 : memref<i32> loc("C:\\Users\\NecC\\source\\repos\\GitLab\\CheewinC\\Learning\\ApiLearning\\Controllers\\ValuesController.cs" :34 :27)
br ^0

^0: // ExitBlock
return

}
